import json
import pdb
import pprint
import datetime
import collections
cnt = collections.Counter()
f = open("data.json")
obj = json.load(f)
for ent in obj['log']['entries']:
    r = ent['request']
    url = r['url']
    if 'getPlexts' in url :
        res = ent['response']['content']['text']
        p2 = json.loads(str(res))
        for res2 in p2['result']:
            uid = res2[0]
            t = res2[1]
            pl = res2[2]['plext']
            mtype = pl['markup'][0][0] #['player']['plain']
            pdata = pl['markup'][0][1]
            player = pdata['plain']
            text =  pl['markup'][1][1]['plain']
            if len(pl['markup']) < 3:
                continue
            if len(pl['markup'][2]) < 2:
                continue
            ptype = pl['markup'][2][0]
            if ptype != 'PORTAL':
                continue
            portal = pl['markup'][2][1]
            if 'team' not in portal:
                continue
            lat = portal['latE6']
            lon = portal['lngE6']
            team2 = portal['team']
            plain = portal['plain']
            if 'team' in pdata :
                team = pl['markup'][0][1]['team'][0]
                dt = datetime.datetime.fromtimestamp(t/1000)
                cnt[str(plain.encode('utf8','ignore'))  ] += 1
for x in cnt.most_common(20):
    print("\t".join( [str(y) for y in x] ))
